# This file requires a blank line at the end.

dbLoadDatabase("./tCat.dbd",0,0)
tCat_registerRecordDeviceDriver(pdbbase)
callbackSetQueueSize(10000)

tcSetScanRate(10, 5)
tcGenerateList ("C:\SlowControls\TwinCAT3\Sandbox\Target\X1VACTEMP\x1vactemp.txt", "-rv -l -ps")
tcGenerateList ("C:\SlowControls\TwinCAT3\Sandbox\Target\X1VACTEMP\x1vactemp.req", "-rv -lb -ps")
tcGenerateList ("C:\SlowControls\TwinCAT3\Sandbox\Target\X1VACTEMP\x1vactemp.ini", "-rv -l -ns -ps")
tcLoadRecords ("C:\SlowControls\TwinCAT3\Sandbox\Target\X1VACTEMP\PLC1\PLC1.tpy", "-rv")

iocInit()
